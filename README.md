# Bird Image Classification

## Description

This project uses the [525 bird species dataset](https://www.kaggle.com/datasets/gpiosenka/100-bird-species/data)
to build a classification model. The aim is to demonstrate how a container from the
[FAIR-DS workflow containers](https://git.rwth-aachen.de/fair-ds/ap-4-2-demonstrator/workflow-containers)
project can be used to run code on the RWTH HPC cluster.

## Additional Information

The HPC connection is made with the [AixCelenx CI Driver](https://git.rwth-aachen.de/adrianschmitz2/aixcilenz-ci-driver) provided by [Adrian Schmitz](https://git.rwth-aachen.de/adrianschmitz2) and [Felix Tomski](https://git.rwth-aachen.de/felix.tomski).

## Getting Started

### Dependencies

### Installation

### Usage

## Contributing

## License
