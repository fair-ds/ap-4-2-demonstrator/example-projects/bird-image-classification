import argparse
from dotenv import load_dotenv

load_dotenv()

import logging

from dataset import BirdDataset
from model import CNNModel, TinyCNNModel
from training.model_trainer import ModelTrainer

import config


def _parse_cli() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("--epochs", type=int, default=config.EPOCHS)
    parser.add_argument("--batch_size", type=int, default=config.BATCH_SIZE)
    parser.add_argument("--lr", type=float, default=0.001)
    parser.add_argument("--momentum", type=float, default=0.9)
    parser.add_argument("--debug", action="store_true", default=config.DEBUG_MODE)
    parser.add_argument("--debug_n_labels", type=int, default=config.DEBUG_N_LABELS)

    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_cli()

    logging.basicConfig(level=logging.INFO)

    training_dataset = BirdDataset("train")
    validation_dataset = BirdDataset("valid")

    if args.debug:
        model = TinyCNNModel(training_dataset.n_labels)
    else:
        model = CNNModel(training_dataset.n_labels)

    trainer = ModelTrainer(
        model=model,
        lr=args.lr,
        momentum=args.momentum,
        batch_size=args.batch_size,
    )

    trainer.train(training_dataset, validation_dataset, epochs=args.epochs)

    # Save plots of the metrics and label accuracies to disk
    trainer.get_metrics_plot().savefig("metrics_plot.png")
    trainer.get_label_accuracy_plot(BirdDataset(data_set=None)).savefig(
        "label_accuracies.png"
    )
