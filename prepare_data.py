try:
    from dotenv import load_dotenv

    load_dotenv()
except ImportError:
    pass

import logging
from pathlib import Path
import zipfile

import kaggle
import tqdm

logger = logging.getLogger(__name__)

DATASET_NAME = "wenewone/cub2002011"
DOWNLOAD_FILE_NAME = Path(DATASET_NAME.split("/")[-1] + ".zip")
DOWNLOAD_DIRECTORY = Path(DATASET_NAME.split("/")[-1])


def download_data():
    """ """
    logger.info("Starting data download...")

    # Check if the data is already downloaded
    if DOWNLOAD_FILE_NAME.exists() or DOWNLOAD_DIRECTORY.exists():
        logger.info("Data already downloaded.")
        return

    # Download the data
    logger.info("Downloading data from %s...", DATASET_NAME)
    kaggle.api.dataset_download_files(DATASET_NAME, quiet=False)

    if not Path(DOWNLOAD_FILE_NAME).exists():
        logger.error("File %s not found.", DOWNLOAD_FILE_NAME)
        raise FileNotFoundError(f"File {DOWNLOAD_FILE_NAME} not found.")

    logger.info("Data download completed.")


def extract_data():
    """ """
    logger.info("Starting data extraction...")

    # Check if the data is already extracted
    if DOWNLOAD_DIRECTORY.exists():
        logger.info("Data already extracted.")
        return

    # Extract the data
    logger.info("Extracting data from %s to %s", DOWNLOAD_FILE_NAME, DOWNLOAD_DIRECTORY)
    with zipfile.ZipFile(DOWNLOAD_FILE_NAME, "r") as zip_ref:
        # Get the total number of entries in the zip file to set up the progress bar
        total = len(zip_ref.infolist())
        with tqdm.tqdm(total=total, unit="files") as pbar:
            for member in zip_ref.infolist():
                # Extract each file and update the progress bar
                zip_ref.extract(member, DOWNLOAD_DIRECTORY)
                pbar.update()

    # Delete the zip file
    logger.info("Deleting zip file %s", DOWNLOAD_FILE_NAME)
    DOWNLOAD_FILE_NAME.unlink()

    logger.info("Data extraction completed.")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    download_data()
    extract_data()
