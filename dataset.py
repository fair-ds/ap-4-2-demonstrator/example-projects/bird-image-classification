"""
This file contains the dataloader for the dataset.
"""

from pathlib import Path
import random

import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image
import torch
from torchvision import transforms
from torchvision.io import read_image
from torch.utils.data import Dataset

from config import DATASET_DIRECTORY, DEBUG_MODE, DEBUG_N_LABELS


def build_label_file(directory=DATASET_DIRECTORY):
    directory = Path(directory)

    with open(directory / "CUB_200_2011/classes.txt") as f:
        classes = pd.read_csv(
            f, sep=" ", header=None, index_col=0, names=["class_name"]
        )

    with open(directory / "CUB_200_2011/train_test_split.txt") as f:
        train_test_split = pd.read_csv(
            f, sep=" ", header=None, index_col=0, names=["is_train"]
        )

    with open(directory / "CUB_200_2011/image_class_labels.txt") as f:
        image_class_labels = pd.read_csv(
            f, sep=" ", header=None, index_col=0, names=["class_id"]
        )

    with open(directory / "CUB_200_2011/images.txt") as f:
        images = pd.read_csv(f, sep=" ", header=None, index_col=0, names=["image_path"])

    df = (
        images.merge(image_class_labels, left_index=True, right_index=True)
        .merge(classes, left_on="class_id", right_index=True)
        .merge(train_test_split, left_index=True, right_index=True)
    )

    # If we're in DEBUG_MODE, limit the number of labels to DEBUG_N_LABELS
    if DEBUG_MODE:
        print(f"droping all but {DEBUG_N_LABELS} labels for debugging")
        random.seed(42)
        debug_labels = random.choices(df["class_id"].unique(), k=DEBUG_N_LABELS)
        df = df[df["class_id"].isin(debug_labels)]

        # Reindex the class_id to be 0, 1, 2, ...
        df["class_id"] = pd.Categorical(df["class_id"]).codes

    return df


def preview_dataset():
    train_dataset = BirdDataset("train")
    train_dataset.transform = None
    figure = plt.figure(figsize=(10, 10))

    cols, rows = 3, 3
    image_indexes = random.sample(range(len(train_dataset)), cols * rows)
    for i, idx in enumerate(image_indexes, start=1):
        img, label = train_dataset[idx]
        figure.add_subplot(rows, cols, i)
        plt.title(LABEL_MAP[float(label)])
        plt.axis("off")
        plt.imshow(img.permute(1, 2, 0))
    plt.show()


class BirdDataset(Dataset):
    def __init__(self, data_set: str, label_df=None):
        # Make a copy of the label_df so we don't modify the original
        label_df = label_df if label_df is not None else build_label_file()

        if data_set is None:
            pass
        elif data_set == "train":
            label_df = label_df[label_df["is_train"] == 1]
        elif data_set == "valid":
            label_df = label_df[label_df["is_train"] == 0]
        else:
            raise ValueError("data_set must be 'train', 'valid' or None")

        self.labels = label_df["class_id"].values
        self.n_labels = len(self.labels)
        self.label_crosswalk = (
            label_df[["class_id", "class_name"]]
            .drop_duplicates()
            .set_index("class_id")
            .to_dict()["class_name"]
        )
        self.image_paths = label_df["image_path"].values

        self.transform = transforms.Compose(
            [
                transforms.Resize((224, 224)),
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]
        )

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        image_path = DATASET_DIRECTORY / "CUB_200_2011/images" / self.image_paths[idx]
        image = Image.open(image_path).convert("RGB")
        if self.transform:
            image = self.transform(image)
        label = torch.tensor(self.labels[idx], dtype=torch.long)
        return image, label

    def calculate_label_accuracies(self, model, text_labels=False) -> dict:
        model.eval()
        num_correct = {label: 0 for label in self.labels}
        num_total = {label: 0 for label in self.labels}

        with torch.no_grad():
            for idx in range(len(self)):
                image, label = self[idx]
                image = image.unsqueeze(0)
                output = model(image)
                _, prediction = torch.max(output, 1)

                label = label.item()
                num_total[label] += 1
                num_correct[label] += int(prediction == label)

        if text_labels:
            return {
                self.label_crosswalk[label]: num_correct[label] / num_total[label]
                for label in self.labels
            }

        return {label: num_correct[label] / num_total[label] for label in self.labels}
