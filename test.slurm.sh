#!/usr/bin/zsh 

#SBATCH --ntasks=1
#SBATCH --time=00:01:00
#SBATCH --job-name=fair-ds_hpc-test

echo "Loading Modules..."
module load GCCcore/.13.3.0
module load Python/3.12.3
module load GCC/12.3.0
module load OpenMPI/4.1.5
module load PyTorch/2.1.2-CUDA-12.1.1

echo "Executing Script..."
python main.py --debug