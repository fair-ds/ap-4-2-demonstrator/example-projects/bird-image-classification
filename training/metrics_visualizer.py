import matplotlib.pyplot as plt


class MetricsVisualizer:
    def __init__(self, metrics_list):
        self.metrics_list = metrics_list

    def plot_metrics(self):
        _, (ax1, ax2) = plt.subplots(2, figsize=(10, 8))

        ax1.plot([m.training_loss for m in self.metrics_list], label="Training Loss")
        ax1.plot(
            [m.validation_loss for m in self.metrics_list], label="Validation Loss"
        )
        ax1.set_title("Loss")
        ax1.legend()

        ax2.plot(
            [m.training_accuracy for m in self.metrics_list], label="Training Accuracy"
        )
        ax2.plot(
            [m.validation_accuracy for m in self.metrics_list],
            label="Validation Accuracy",
        )
        ax2.set_title("Accuracy")
        ax2.legend()

        plt.subplots_adjust(hspace=0.5)

        return plt

    def plot_label_accuracy(self, dataset, model):
        # Calculate label accuracies
        label_accuracies = dataset.calculate_label_accuracies(model, text_labels=True)

        # Sort labels by accuracy
        sorted_labels = sorted(label_accuracies.items(), key=lambda x: x[1])

        # Select top 3, bottom 3, and average of the rest
        top_3 = sorted_labels[-3:]
        bottom_3 = sorted_labels[:3]

        labels = [label for label, _ in top_3 + bottom_3]
        accuracies = [acc for _, acc in top_3 + bottom_3]

        if len(sorted_labels) > 6:
            rest = sorted_labels[3:-3]
            average_rest = sum(acc for _, acc in rest) / len(rest)
            labels += ["Average"]
            accuracies += [average_rest]

        # Create bar plot
        plt.figure(figsize=(10, 6))
        plt.bar(labels, accuracies)
        plt.title("Label Accuracies")
        plt.xticks(rotation=45)

        return plt
