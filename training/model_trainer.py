from dataclasses import dataclass
import logging

import torch
from torch import nn
from torch.utils.data import DataLoader
import tqdm

from .metrics_visualizer import MetricsVisualizer

logger = logging.getLogger(__name__)

DEFAULT_OPTIMIZER = torch.optim.SGD
DEFAULT_CRITERION = nn.CrossEntropyLoss()


@dataclass
class Metrics:
    training_loss: float
    training_accuracy: float
    # training_label_metrics: dict
    validation_loss: float
    validation_accuracy: float
    # validation_label_metrics: dict


class ModelTrainer:
    def __init__(
        self,
        model,
        optimizer=torch.optim.SGD,
        criterion=nn.CrossEntropyLoss(),
        batch_size=32,
        device=None,
        **kwargs,
    ):
        self.model = model
        self.device = device
        if self.device is None:
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.optimizer = optimizer(model.parameters(), **kwargs)
        self.criterion = criterion
        self.batch_size = batch_size

        self.metrics = []

    def get_metrics_plot(self):
        return MetricsVisualizer(self.metrics).plot_metrics()

    def get_label_accuracy_plot(self, dataset):
        return MetricsVisualizer(self.metrics).plot_label_accuracy(dataset, self.model)

    def train(self, training_dataset, validation_dataset, epochs=1):
        training_loader = DataLoader(
            training_dataset, batch_size=self.batch_size, shuffle=True
        )
        validation_loader = DataLoader(
            validation_dataset, batch_size=self.batch_size, shuffle=False
        )

        for epoch in range(epochs):
            logger.info(f"Epoch {epoch + 1}")

            training_loss, training_accuracy = self.train_one_epoch(training_loader)
            validation_loss, validation_accuracy = self.validate(validation_loader)

            self.metrics.append(
                Metrics(
                    training_loss=training_loss,
                    training_accuracy=training_accuracy,
                    validation_loss=validation_loss,
                    validation_accuracy=validation_accuracy,
                )
            )

    def train_one_batch(self, inputs, labels):
        inputs, labels = inputs.to(self.device), labels.to(self.device)
        self.optimizer.zero_grad()

        outputs = self.model(inputs)
        _, predictions = torch.max(outputs, 1)
        loss = self.criterion(outputs, labels)

        loss.backward()
        self.optimizer.step()

        return loss, predictions, inputs.size(0)

    def validate_one_batch(self, inputs, labels):
        inputs, labels = inputs.to(self.device), labels.to(self.device)

        outputs = self.model(inputs)
        _, predictions = torch.max(outputs, 1)
        loss = self.criterion(outputs, labels)

        return loss, predictions, inputs.size(0)

    def calculate_metrics(self, running_loss, correct, total_samples):
        current_loss = running_loss / total_samples
        current_accuracy = float(correct.double() / total_samples)
        return current_loss, current_accuracy

    def train_one_epoch(self, training_loader):
        self.model.train()
        running_loss = 0.0
        correct = 0
        total_samples = 0

        with tqdm.tqdm(
            total=len(training_loader.dataset), desc="Training", unit=" samples"
        ) as progress_bar:
            for inputs, labels in training_loader:
                loss, predictions, batch_size = self.train_one_batch(inputs, labels)

                running_loss += loss.item() * batch_size
                correct += torch.sum(predictions == labels.data)
                total_samples += batch_size

                current_loss, current_accuracy = self.calculate_metrics(
                    running_loss, correct, total_samples
                )

                progress_bar.set_postfix(
                    {"loss": current_loss, "accuracy": current_accuracy}
                )
                progress_bar.update(batch_size)

        return current_loss, current_accuracy

    def validate(self, validation_loader):
        self.model.eval()
        running_loss = 0.0
        correct = 0
        total_samples = 0

        with torch.no_grad():
            with tqdm.tqdm(
                total=len(validation_loader.dataset), desc="Validation", unit="sample"
            ) as progress_bar:
                for inputs, labels in validation_loader:
                    loss, predictions, batch_size = self.validate_one_batch(
                        inputs, labels
                    )

                    running_loss += loss.item() * batch_size
                    correct += torch.sum(predictions == labels.data)
                    total_samples += batch_size

                    current_loss, current_accuracy = self.calculate_metrics(
                        running_loss, correct, total_samples
                    )

                    progress_bar.set_postfix(
                        {"loss": current_loss, "accuracy": current_accuracy}
                    )
                    progress_bar.update(batch_size)

        return current_loss, current_accuracy
