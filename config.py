from pathlib import Path
import random

import pandas as pd

# === USER PROVIDED ===
DATASET_DIRECTORY = Path("cub2002011")
DEBUG_MODE = False
DEBUG_N_LABELS = 10
EPOCHS = 5
BATCH_SIZE = 32
